---
marp: true
paginate: true
math: katex
---

<script src="https://unpkg.com/mermaid@9.1.7/dist/mermaid.min.js"></script>
<script>mermaid.initialize({startOnLoad:true});</script>

<style global>
section::after {
  content: attr(data-marpit-pagination) ' / ' attr(data-marpit-pagination-total);
}
</style>

<header>

# Formation net7

</header>

![bg 50%](../2021-2022/figures/git.svg)

<footer>
2022-2023, Fainsin Laurent
</footer>

---

<header>

# Introduction

</header>

##  Git, c'est quoi ?

- Un gestionnaire de versions décentralisé
- Créé en 2005 par Linus Torvalds, le créateur de Linux 
- Gratuit et [open source](https://github.com/git/git) (GPLv2)
- Rapide, fiable et scalable
- [Incontournable](https://trends.google.com/trends/explore?q=git,svn,mercurial), surtout dans le monde professionnel
- Un graphe !

---

<header>

# Introduction

</header>

## [Git, pourquoi ?](https://www.pierre-giraud.com/git-github-apprendre-cours/presentation-git-github/)

Exemple d'un petit site web (genre eportfolio 👀)

---

<header>

# Introduction

</header>

<center style="padding-bottom: 30rem;">

## Décentralisé ?

</center>

![bg 90%](https://upload.wikimedia.org/wikipedia/commons/b/ba/Centralised-decentralised-distributed.png)

---

<header>

# Introduction

</header>

## [Protocoles de communication](https://git-scm.com/book/en/v2/Git-on-the-Server-The-Protocols)

Git peut utiliser quatre protocoles distincts pour transférer des données:

- Local: `file://`
- HTTP: `http(s)://` (port 80)
- Secure Shell: `ssh://` (port 22)
- Git: `git://` (port 9418)

---

<header>

# Introduction

</header>

<center>

## Un Graphe ?

</center>

<div class="mermaid">
%%{init: { 'theme': 'base' } }%%
gitGraph
  commit
  branch hotfix
  checkout hotfix
  commit
  branch develop
  checkout develop
  commit
  branch featureB
  checkout featureB
  commit
  checkout main
  checkout hotfix
  commit
  checkout develop
  commit
  checkout featureB
  commit
  checkout main
  merge hotfix
  checkout featureB
  commit
  checkout develop
  branch featureA
  commit
  checkout develop
  merge hotfix
  checkout featureA
  commit
  checkout featureB
  commit
  checkout develop
  merge featureA
  branch release
  checkout release
  commit
  checkout main
  commit
  checkout release
  merge main
  checkout develop
  merge release
  checkout main
  merge develop
  commit
</div>

---

<header>

# Introduction

</header>

## [À quoi ça ressemble git ?](https://education.github.com/git-cheat-sheet-education.pdf)

$\rightarrow$ Un outil en ligne de commande

---

<header>

# Installation de Git

</header>

## [Windows](https://git-scm.com/download/win)

`choco install git`
ou
`winget install --id Git.Git -e --source winget`
ou
Télécharger l'installeur et l'exécuter

---

<header>

# Installation de Git

</header>

## [Mac](https://git-scm.com/download/mac)

`brew install git`
ou
`sudo port install git`

---

<header>

# Installation de Git

</header>

## [Linux](https://git-scm.com/download/linux)

`sudo apt install git-all` (ubuntu/debian)
ou
`sudo pacman -S git` (arch linux)

---

<header>

# [Configuration de Git](https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup)

</header>

Verification de l'installation:
`git --version`

Configurer son identité:
`git config --global user.name "Laurent Fainsin"`
`git config --global user.email "laurent@fainsin.bzh"`

Configurer son éditeur favoris:
`git config --global core.editor nvim`

[Autres paramètres](https://git-scm.com/docs/git-config#_variables):
`git config --list`

---

<header>

# Les bases de Git

</header>

# [Les quatre phases de git](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository#_recording_changes_to_the_repository)

<div class="mermaid">
sequenceDiagram
  participant Untracked
  participant Unmodified
  participant Modified
  participant Staged
  Untracked->>Staged: Add the file
  Unmodified->>Modified: Edit the file
  Modified->>Staged: Stage the file
  Unmodified->>Untracked: Remove the file
  Staged->>Unmodified: Commit
</div>

---

<header>

# Les bases de Git

</header>

## [Initialisation d'un dépôt dans un répertoire existant](https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository#_initializing_a_repository_in_an_existing_directory)

Naviguer vers le répertoire existant:
`cd /le/chemin/vers/mon/projet/`

Initialiser le dépôt:
`git init`

---

<header>

# Les bases de Git

</header>

## [Cloner un dépôt existant](https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository#_git_cloning)

Naviguer vers là où on souhaite avoir le dépôt:
`cd /le/chemin/vers/mon/dossier/`

Clone le dépôt (distant):
`git clone https://github.com/samuelmarina/is-even`
ou
`git clone https://github.com/samuelmarina/is-even est-pair`

---

<header>

# Les bases de Git

</header>

## [Vérifier le statut des fichiers](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository#_checking_status)

```
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
nothing to commit, working tree clean
```

```
$ echo 'My Project' > README
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
Untracked files:
  (use "git add <file>..." to include in what will be committed)

    README

nothing added to commit but untracked files present (use "git add" to track)
```

---

<header>

# Les bases de Git

</header>

## [Indexer des nouveaux fichiers](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository#_tracking_files)

```
$ git add README
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)

    new file:   README
```

---

<header>

# Les bases de Git

</header>

## [Indexer des fichiers modifiés](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository#_staging_modified_files)

```
$ nvim CONTRIBUTING.md
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

    new file:   README

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

    modified:   CONTRIBUTING.md
```

---

<header>

# Les bases de Git

</header>

## [Indexer des fichiers modifiés](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository#_staging_modified_files)

```
$ git add CONTRIBUTING.md
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

    new file:   README
    modified:   CONTRIBUTING.md
```

---

<header>

# Les bases de Git

</header>

## [Indexer des fichiers modifiés](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository#_staging_modified_files)

```
$ nvim CONTRIBUTING.md
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

    new file:   README
    modified:   CONTRIBUTING.md

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

    modified:   CONTRIBUTING.md
```

---

<header>

# Les bases de Git

</header>

## [Indexer des fichiers modifiés](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository#_staging_modified_files)

```
$ git add CONTRIBUTING.md
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

    new file:   README
    modified:   CONTRIBUTING.md
```

---

<header>

# Les bases de Git

</header>

## [Supprimer des fichiers](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository#_removing_files)

```
$ rm PROJECTS.md
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes not staged for commit:
  (use "git add/rm <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        deleted:    PROJECTS.md

no changes added to commit (use "git add" and/or "git commit -a")
```

---

<header>

# Les bases de Git

</header>

## [Supprimer des fichiers](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository#_removing_files)

```
$ git rm PROJECTS.md
rm 'PROJECTS.md'
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

    deleted:    PROJECTS.md
```

---

<header>

# Les bases de Git

</header>

## [Déplacer des fichiers](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository#_git_mv)

```
$ git mv README.md README
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

    renamed:    README.md -> README
```

---

<header>

# Les bases de Git

</header>

## [Déindexer des fichiers indexés](https://git-scm.com/book/en/v2/Git-Basics-Undoing-Things#_unstaging)

```
$ git add *
$ git status
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

    renamed:    README.md -> README
    modified:   CONTRIBUTING.md
```

---

<header>

# Les bases de Git

</header>

## [Déindexer des fichiers indexés](https://git-scm.com/book/en/v2/Git-Basics-Undoing-Things#_unstaging)

```
$ git reset HEAD CONTRIBUTING.md
Unstaged changes after reset:
M	CONTRIBUTING.md
```

```
$ git status
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

    renamed:    README.md -> README

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

    modified:   CONTRIBUTING.md
```

---

<header>

# Les bases de Git

</header>

## [Démodifier des fichiers modifiés](https://git-scm.com/book/en/v2/Git-Basics-Undoing-Things#_unstaging)

```
$ git status
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

    renamed:    README.md -> README

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

    modified:   CONTRIBUTING.md
```

```
$ git checkout -- CONTRIBUTING.md
$ git status
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

    renamed:    README.md -> README
```

---

<header>

# Les bases de Git

</header>

## [Valider ses modifications](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository#_committing_changes)

```
$ git commit
```

```python
# Please enter the commit message for your changes. Lines starting
# with '#' will be ignored, and an empty message aborts the commit.
# On branch master
# Your branch is up-to-date with 'origin/master'.
#
# Changes to be committed:
#	new file:   README
#	modified:   CONTRIBUTING.md
#
~
~
~
".git/COMMIT_EDITMSG" 9L, 283C
```

---

<header>

# Les bases de Git

</header>

## [Valider ses modifications](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository#_committing_changes)


```
$ git commit -m "Story 182: fix benchmarks for speed"
[master 463dc4f] Story 182: fix benchmarks for speed
 2 files changed, 2 insertions(+)
 create mode 100644 README
```

---

<header>

# Les bases de Git

</header>

## [Revenir un commit en arrière](https://git-scm.com/book/en/v2/Git-Basics-Undoing-Things#_undoing)

```
$ git commit -m 'Initial commit'
$ git add forgotten_file
$ git commit --amend
```

---

<header>

# Les bases de Git

</header>

## [Déplacer des fichiers](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository#_git_mv)

```
$ mv README.md README
$ git rm README.md
$ git add README
```

---

<header>

# Les bases de Git

</header>

## [.gitignore](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository#_ignoring) [files](https://github.com/github/gitignore)

```python
# ignore all .a files
*.a

# but do track lib.a, even though you're ignoring .a files above
!lib.a

# only ignore the TODO file in the current directory, not subdir/TODO
/TODO

# ignore all files in any directory named build
build/

# ignore doc/notes.txt, but not doc/server/arch.txt
doc/*.txt

# ignore all .pdf files in the doc/ directory and any of its subdirectories
doc/**/*.pdf
```

---

<header>

# Les bases de Git

</header>

## [git diff](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository#_git_diff_staged)

```diff
$ git diff
diff --git a/CONTRIBUTING.md b/CONTRIBUTING.md
index 8ebb991..643e24f 100644
--- a/CONTRIBUTING.md
+++ b/CONTRIBUTING.md
@@ -65,7 +65,8 @@ branch directly, things can get messy.
 Please include a nice description of your changes when you submit your PR;
 if we have to read the whole diff to figure out why you're contributing
 in the first place, you're less likely to get feedback and have your change
-merged in.
+merged in. Also, split your changes into comprehensive chunks if your patch is
+longer than a dozen lines.

 If you are starting to work on a particular area, feel free to submit a PR
 that highlights your work in progress (and note in the PR title that it's
```

---

<header>

# Les bases de Git

</header>

## [git log](https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository#_viewing_history)

```
$ git log
commit 085bb3bcb608e1e8451d4b2432f8ecbe6306e7e7
Author: Scott Chacon <schacon@gee-mail.com>
Date:   Sat Mar 15 16:40:33 2008 -0700

    Remove unnecessary test

commit a11bef06a3f659402fe7563abf99ad00de2209e6
Author: Scott Chacon <schacon@gee-mail.com>
Date:   Sat Mar 15 10:31:28 2008 -0700

    Initial commit
```

---

<header>

# Les branches dans Git

</header>

## [Créer une branche](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging#_basic_branching)

```
$ git checkout -b iss53
Switched to a new branch "iss53"
```
ou
```
$ git branch iss53
$ git checkout iss53
```

<div class="mermaid">
%%{init: { 'theme': 'base' } }%%
gitGraph
  commit
  commit
  commit
  branch iss53
  commit type:HIGHLIGHT id: "‎"
</div>

---

<header>

# Les branches dans Git

</header>

## [Travailler sur la nouvelle branche](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging)

```
$ vim index.html
$ git commit -a -m 'Create new footer [issue 53]'
```

<div class="mermaid">
%%{init: { 'theme': 'base' } }%%
gitGraph
  commit
  commit
  commit
  branch iss53
  commit
</div>

---

<header>

# Les branches dans Git

</header>

## [Changer de branche](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging)

```
$ git checkout main
Switched to branch 'main'
```

<div class="mermaid">
%%{init: { 'theme': 'base' } }%%
gitGraph
  commit
  commit
  commit type:HIGHLIGHT id: "‎"
  branch iss53
  commit
  checkout main
</div>

---

<header>

# Les branches dans Git

</header>

## [Encore plus de branches](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging)

```
$ git checkout -b hotfix
Switched to a new branch 'hotfix'
$ vim index.html
$ git commit -a -m 'Fix broken email address'
[hotfix 1fb7853] Fix broken email address
 1 file changed, 2 insertions(+)
```

<div class="mermaid">
%%{init: { 'theme': 'base' } }%%
gitGraph
  commit
  commit
  commit
  branch iss53
  commit
  checkout main
  branch hotfix
  commit
</div>

---

<header>

# Les branches dans Git

</header>

## [Fusionner des branches](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging)

```
$ git checkout master
$ git merge hotfix
Updating f42c576..3a0874c
Fast-forward
 index.html | 2 ++
 1 file changed, 2 insertions(+)
```

<div class="mermaid">
%%{init: { 'theme': 'base' } }%%
gitGraph
  commit
  commit
  commit
  branch iss53
  commit
  checkout main
  branch hotfix
  commit
  checkout main
  merge hotfix
</div>

---

<header>

# Les branches dans Git

</header>

## [Fusionner des branches](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging)

```
$ git checkout iss53
$ vim index.html
$ git commit -a -m 'Finish the new footer [issue 53]'
$ git checkout main
$ git merge iss53
```

<div class="mermaid">
%%{init: { 'theme': 'base' } }%%
gitGraph
  commit
  commit
  commit
  branch iss53
  commit
  checkout main
  branch hotfix
  commit
  checkout main
  merge hotfix
  checkout iss53
  commit
  checkout main
  merge iss53
</div>

---

<header>

# Les branches dans Git

</header>

## [Supprimer des branches](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging)

```
$ git branch -d hotfix
Deleted branch hotfix (3a0874c).
```

---

<header>

# Les conflits dans Git

</header>

## [Conflit lors d'un merge](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging)

```
$ git merge iss53
Auto-merging index.html
CONFLICT (content): Merge conflict in index.html
Automatic merge failed; fix conflicts and then commit the result.
```

```
$ git status
On branch master
You have unmerged paths.
  (fix conflicts and run "git commit")

Unmerged paths:
  (use "git add <file>..." to mark resolution)
    both modified:      index.html

no changes added to commit (use "git add" and/or "git commit -a")
```

---

<header>

# Les conflits dans Git

</header>

## [Résoudre un conflit](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging)

```
<<<<<<< HEAD:index.html
<div id="footer">contact : email.support@github.com</div>
=======
<div id="footer">
 please contact us at support@github.com
</div>
>>>>>>> iss53:index.html
```

```
<div id="footer">
please contact us at email.support@github.com
</div>
```

`$ git mergetool`

---

<header>

# Le tapis de Git

</header>

## [Mettre sous le tapis ses modifications](https://git-scm.com/book/fr/v2/Utilitaires-Git-Remisage-et-nettoyage)

```
$ git status
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	modified:   index.html

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   lib/simplegit.rb
```

---

<header>

# Le tapis de Git

</header>

## [Mettre sous le tapis ses modifications](https://git-scm.com/book/fr/v2/Utilitaires-Git-Remisage-et-nettoyage)

```
$ git stash
Saved working directory and index state \
  "WIP on master: 049d078 Create index file"
HEAD is now at 049d078 Create index file
(To restore them type "git stash apply")
```

```
$ git status
# On branch master
nothing to commit, working directory clean
```

---

<header>

# Le tapis de Git

</header>

## [Lister ses modifications sous le tapis](https://git-scm.com/book/fr/v2/Utilitaires-Git-Remisage-et-nettoyage)

```
$ git stash list
stash@{0}: WIP on master: 049d078 Create index file
stash@{1}: WIP on master: c264051 Revert "Add file_size"
stash@{2}: WIP on master: 21d80a5 Add number to log
```

---

<header>

# Le tapis de Git

</header>

## [Récuperer ses modifications sous le tapis](https://git-scm.com/book/fr/v2/Utilitaires-Git-Remisage-et-nettoyage)

`git stash apply <stash-name>`
`git stash pop <stash-name>`

```
$ git stash apply
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   index.html
	modified:   lib/simplegit.rb

no changes added to commit (use "git add" and/or "git commit -a")
```

---

<header>

# Le tapis de Git

</header>

## [Passer le balais sous le tapis](https://git-scm.com/book/fr/v2/Utilitaires-Git-Remisage-et-nettoyage)

`git stash drop <stash-name>`

```
$ git stash drop stash@{0}
Dropped stash@{0} (364e91f3f268f0900bc3ee613f9f733e82aaed43)
```

---

<header>

# La collaboration avec Git

</header>

## [Afficher les dépôts distants](https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes#_showing_your_remotes)

```
$ git clone https://github.com/schacon/ticgit && cd ticgit
Cloning into 'ticgit'...
remote: Reusing existing pack: 1857, done.
remote: Total 1857 (delta 0), reused 0 (delta 0)
Receiving objects: 100% (1857/1857), 374.35 KiB | 268.00 KiB/s, done.
Resolving deltas: 100% (772/772), done.
Checking connectivity... done.
```

```
$ git remote -v
origin	https://github.com/schacon/ticgit (fetch)
origin	https://github.com/schacon/ticgit (push)
```

---

<header>

# La collaboration avec Git

</header>

## Plusieurs dépôts distants ?

```
$ git remote -v
bakkdoor  https://github.com/bakkdoor/grit (fetch)
bakkdoor  https://github.com/bakkdoor/grit (push)
cho45     https://github.com/cho45/grit (fetch)
cho45     https://github.com/cho45/grit (push)
defunkt   https://github.com/defunkt/grit (fetch)
defunkt   https://github.com/defunkt/grit (push)
koke      git://github.com/koke/grit.git (fetch)
koke      git://github.com/koke/grit.git (push)
origin    git@git.inpt.fr:fainsil/grit.git (fetch)
origin    git@git.inpt.fr:fainsil/grit.git (push)
```

---

<header>

# La collaboration avec Git

</header>

## [Ajouter un dépôt distant](https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes#_adding_remote_repositories)

`git remote add <shortname> <url>`

```
$ git remote add pb https://github.com/paulboone/ticgit
$ git remote -v
origin	https://github.com/schacon/ticgit (fetch)
origin	https://github.com/schacon/ticgit (push)
pb	https://github.com/paulboone/ticgit (fetch)
pb	https://github.com/paulboone/ticgit (push)
```

---

<header>

# La collaboration avec Git

</header>

## [Se synchroniser avec un dépôt distant](https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes#_fetching_and_pulling)

`git fetch <remote-name>`

```
$ git fetch pb
remote: Counting objects: 43, done.
remote: Compressing objects: 100% (36/36), done.
remote: Total 43 (delta 10), reused 31 (delta 5)
Unpacking objects: 100% (43/43), done.
From https://github.com/paulboone/ticgit
 * [new branch]      master     -> pb/master
 * [new branch]      ticgit     -> pb/ticgit
```

---

<header>

# La collaboration avec Git

</header>

## [Pousser son travail sur un dépôt distant](https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes#_pushing_remotes)

`git push <remote> <branch>`

```
$ git push origin master
Enumerating objects: 8, done.
Counting objects: 100% (8/8), done.
Delta compression using up to 4 threads.
Compressing objects: 100% (4/4), done.
Writing objects: 100% (8/8), 642 bytes | 160.00 KiB/s, done.
Total 8 (delta 0), reused 0 (delta 0)
To github.com: thesparkler/git-tutorials.git
 * [new branch]       master -> master
Branch 'master' set up to track remote branch 'master' from 'origin'.
```

---

<header>

# La collaboration avec Git

</header>

## [Inspecter un dépôt distant](https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes#_inspecting_remote)

`git remote show <remote>`

```
$ git remote show origin
* remote origin
  Fetch URL: https://github.com/schacon/ticgit
  Push  URL: https://github.com/schacon/ticgit
  HEAD branch: master
  Remote branches:
    master                               tracked
    dev-branch                           tracked
  Local branch configured for 'git pull':
    master merges with remote master
  Local ref configured for 'git push':
    master pushes to master (up to date)
```

---

<header>

# La collaboration avec Git

</header>

## [Supprimer/Renommer un dépôt distant](https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes#_renaming_and_removing_remotes)

```
$ git remote rename pb paul
$ git remote
origin
paul
```

```
$ git remote remove paul
$ git remote
origin
```

---

<header>

# Aller plus loin

</header>

## Les frontends

- [GitHub](https://github.com/)
- [GitLab](https://gitlab.com)
- [Gitea](https://gitea.io/)
- [Gogs](https://gogs.io/)
- [Codeberg](https://codeberg.org/)
- [SourceHut](https://sr.ht/)

---

<header>

# Aller plus loin

</header>

## Quelques outils

- [pre-commit](https://pre-commit.com/)
- [GitKraken](https://www.gitkraken.com/)
- [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)
- [CI/CD](https://resources.github.com/ci-cd/)
- [Signature](https://docs.github.com/en/authentication/managing-commit-signature-verification/signing-commits)
