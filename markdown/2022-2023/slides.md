---
marp: true
paginate: true
math: katex
---

<script src="https://unpkg.com/mermaid@9.1.7/dist/mermaid.min.js"></script>
<script>mermaid.initialize({startOnLoad:true});</script>

<style>
section::after {
  content: attr(data-marpit-pagination) ' / ' attr(data-marpit-pagination-total);
}
</style>

<header>

# Formation net7

</header>

![bg 50%](https://upload.wikimedia.org/wikipedia/commons/4/48/Markdown-mark.svg)

<footer>
2022-2023, Fainsin Laurent
</footer>

---

<header>

# Introduction

</header>

##  Markdown, c'est quoi ?

- Un langage de balisage 
- Créé en 2004 par John Gruber et Aaron Swartz
- Gratuit et [open source](https://github.com/git/git) (~BSD)
- Léger, facile à apprendre
- Traduisible en d'autres formats: HTML, pdf...

---

<header>

# Introduction

</header>

##  Markdown, pourquoi ?

- "Content first", contrairement au $\LaTeX$ ou au HTML
- Permet d'expérimenter rapidement
- Utilisé partout

---

# [Bases syntaxique](https://www.markdownguide.org/basic-syntax/)

---

# [Syntaxe étendue](https://www.markdownguide.org/extended-syntax/)

---

<header>

# Aller plus loin

</header>

## Intégration d'outils

- [Katex](https://github.com/mundimark/awesome-markdown)
- [Mermaid](https://mermaid-js.github.io/)
- [Plantuml](https://plantuml.com/fr/)
- [MarkMap](https://markmap.js.org/)

---

<header>

# Aller plus loin

</header>

## Des outils qui utilisent markdown

- [Pandoc](https://pandoc.org/)
- [Marp](https://marp.app/)
- [Obsidian](https://obsidian.md/)
- [GitHub](https://github.github.com/gfm/)
- [Wiki.JS](https://docs.requarks.io/en/editors/markdown)
- La majorité des générateurs de sites statiques

---

<header>

# Aller plus loin

</header>

# [Awesome Markdown](https://github.com/mundimark/awesome-markdown)