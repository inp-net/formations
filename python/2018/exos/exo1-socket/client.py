""" client.py """
import socket


class Client:  # Creation de la classe Client
    def __init__(self):  # Constructeur
        ...
        ...

    def __del__(self):  # Destructeur
        ...

    def start(self):  # fonction principale
        msg_a_envoyer = b""
        while msg_recu != b"fin":
            msg_a_envoyer = input("> ")
            # Peut planter si vous tapez des caractères spéciaux
            msg_a_envoyer = msg_a_envoyer.encode()
            # On envoie le message
            ...
            print(msg_recu.decode()) # Là encore, peut planter s il y a des accents


client = Client()  # initialisation de la classe Server (appel du constructeur)
client.start()  # appel de methode start
del client  # appel du destructeur (non obligatoire)

########################################################
# Block 1 ##############################################
#-----------------------------------------------------#
self.hote = 'localhost'  # ip de la machine local
self.port = 12800

########################################################
# Block 2 ##############################################
#-----------------------------------------------------#
self.main_conn.send(msg_a_envoyer)
# On attend le message de bonne réception
msg_recu = self.main_conn.recv(1024)

########################################################
# Block 3 ##############################################
#-----------------------------------------------------#
# On créé un socket en indiquant qu on utilise le protocole TCP
self.main_conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
self.main_conn.connect((self.hote, self.port))  # se connecte au serveur

########################################################
# Block 4 ##############################################
#-----------------------------------------------------#
print("Fermeture de la connection")
self.main_conn.close()
