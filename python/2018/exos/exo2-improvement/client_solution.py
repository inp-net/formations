#!/usr/bin/env python3

import socket
import argparse
import logging
import hashlib
import base64
from getpass import getpass


class Server:  # Création de la classe Server
    def __init__(self, args):  # Constructeur
        self.host = args.host
        self.port = args.port
        ##  logging
        level = logging.INFO  # met le niveau de log à INFO par default
        if args.debug:  # si en mode debug
            level = logging.DEBUG  # mise au niveau debug des logs
        logging.basicConfig(
            filename=args.output,
            level=level,
            format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        )
        ##  socket
        self.main_conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # instancie un object de la classe socket
        self.main_conn.connect((self.host, self.port))
        # se connecte au socket sur le port et l'ip choisis
        logging.info(
            "Connexion établie avec le serveur sur le port {}".format(self.port))
        ## passwd
        self.secret = args.secret
        self.passwd = ''
        if self.secret:
            self.passwd = hashlib.sha512(getpass('Entrer le mdp du chat : ')\
                .encode()).hexdigest()

    def __del__(self):  # Destructeur
        logging.info("Fermeture de la connection")
        try:
            self.main_conn.close()  # fermeture de la connection avec le serveur
        except AttributeError:
            pass

    def start(self):
        msg_to_send = b""
        while msg_to_send != b"fin":
            msg_to_send = b""
            while msg_to_send == b"":
                msg_to_send = input("> ")
                # Peut planter si vous tapez des caractères spéciaux
                msg_to_send = msg_to_send.encode()
            logging.debug('Envoie à {}:{} {}'.format(
                self.host,
                self.port,
                msg_to_send.decode(),)
            )
            # mise en attente de reception d'un msg (longeur max 1024)
            self.main_conn.send(msg_to_send)
            msg_recv = self.main_conn.recv(1024)
            print(msg_recv.decode())


def run():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--port', '-p', type=int, nargs='?', default=12800,
        help='port de connection'
    )
    parser.add_argument(
        '--host', '-x', type=str, nargs='?', default='localhost',
        help='hôte de connection'
    )
    parser.add_argument(
        '--debug', '-d', action='store_true', default=False,
        help='affiche les logs de debug'
    )
    parser.add_argument(
        '--output', '-o', type=str, default=None,
        help='enregistre les log dans le fichier OUTPUT'
    )
    parser.add_argument(
        '--secret', '-s', action='store_true', default=False,
        help='protège avec un mdp la communication'
    )
    args = parser.parse_args()
    server = Server(args)
    server.start()


if __name__ == '__main__':
    run()
