import getpass  # module pour récupérer le mdp
import hashlib  # module pour hasher le mdp

passwd = getpass.getpass('Entrer le mdp : ')  # on récupère le mdp
hash = hashlib.sha1(passwd.encode())  # on hash le mdp
# il faut encoder en binaire la string avant de la hasher
if hash.hexdigest() == 'e36b97a94f5baeed0e63d14b7f49152816535f41':
	print('success')
else:
	print('fail')
