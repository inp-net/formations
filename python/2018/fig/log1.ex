import logging
def run():
	...
	logging.basicConfig(
		filename=args.output,  # le fichier de log
		level=logging.DEBUG,  # niveau de log (DEBUG/INFO/...)
		format='%(asctime)s - %(name)s - ' \+
			'%(levelname)s - %(message)s',
	)
	# différents niveau de message
	# debug, info, warning, error, critical
	logging.debug("Mon debug")
