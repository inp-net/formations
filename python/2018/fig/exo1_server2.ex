    def start(self):
        self.client_conn, self.infos_conn = self.main_conn.accept()
        ip, port = self.infos_conn
        print("Nouvelle connection sur le port {} avec l'ip {}"
				.format(port, ip))
        msg_recu = b""
        while msg_recu != b"fin":
            msg_recu = self.client_conn.recv(1024)  
            # attend un msg de longeur max 1024
            print(msg_recu.decode())
            self.client_conn.send(b"5 / 5")


server = Server()
# initialisation de la classe Server (appel du constructeur)
server.start()

