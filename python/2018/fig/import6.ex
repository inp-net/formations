# Installe virtualenv
$~ (python3 -m) pip install virtualenv --user
# créé un environnement virtuel dans le dossier env
$~ python3 -m venv env
# on active l'environnement virtuel
$~ source env/bin/activate
