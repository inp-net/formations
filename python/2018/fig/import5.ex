# installation de Django
$~ (python3 -m) pip install Django --user
# installation depuis un fichier
$~ (python3 -m) pip install -r requirement.txt
# Liste les modules
$~ (python3 -m) pip uninstall Django
# Désinstaller un module
$~ (python3 -m) pip uninstall Django
