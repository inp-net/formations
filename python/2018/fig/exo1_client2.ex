    def start(self):
        msg_a_envoyer = b""
        while msg_a_envoyer != b"fin":
            msg_a_envoyer = b""
            while msg_a_envoyer == b"":
                msg_a_envoyer = input("> ")
                # Peut planter si vous tapez des caractères spéciaux
                msg_a_envoyer = msg_a_envoyer.encode()
            # On envoie le message
            self.main_conn.send(msg_a_envoyer)
            msg_recu = self.main_conn.recv(1024)
            print(msg_recu.decode()) 
            # Là encore, peut planter s'il y a des accents


client = Client()  
# initialisation de la classe Server (appel du constructeur)
client.start()  # appel de methode start
del client  # appel du destructeur (non obligatoire)

