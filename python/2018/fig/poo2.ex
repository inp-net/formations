>>> import Verre
>>> volume = 25  # volume du verre
>>> mon_beau_verre = Verre(volume)  # on instancie la classe
>>> mon_beau_verre.contenue
0
>>> mon_beau_verre.remplir(25)  # on appelle la méthode remplir
>>> mon_beau_verre.contenue  # on verifie qu'elle a bien modifié le verre
25
>>> mon_beau_verre.boire(20)  # rebelotte
>>> mon_beau_verre.contenue
5
