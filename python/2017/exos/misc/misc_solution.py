# 1
l = ["aafqzea", "avfa", "aaeaa", "a"]
l.sort(key=len)
print(l)
l.sort(key=lambda x: x.count('a'))
print(l)

# 2
import string
d = {b: c for b, c in zip(string.ascii_lowercase, string.ascii_uppercase[::-1])}
print(d)
