"""
Inverser un fichier texte, non seulement l'ordre des lignes, mais aussi les lettres. Écrire le résultat dans un nouveau fichier, en numérotant chaque lignes.
Exemple :
    "bonjour
     au revoir"
    doit donner :
    "0 riover ua
     1 ruojnob"
"""
