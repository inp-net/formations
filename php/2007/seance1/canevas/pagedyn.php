<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
    <head>
    <title>Page numéro <?php echo($_GET["page"]); ?></title>
        <style>
            @import url('css/style.css');
        </style>
    </head>

    <body>
        <div>
            <ul>
                <li><a href="?page=0&amp;ami=marcel">Page 1</a></li>
                <li><a href="?page=1">Page 2</a></li>
            </ul>
        </div>


<?php
    $pages=array("page1.php","page2.htm");
    $index=0;
    if(isset($_GET["page"])){
        $index=$_GET["page"];
    }
    $fichier=$pages[$index];
    include("includes/".$fichier);
?>

    </body>
</html>
