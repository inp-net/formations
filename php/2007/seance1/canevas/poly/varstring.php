<?php

$nombre=2;
$ami="Roger";
$ami2="Norbert";

# $moi  contient "J'ai 2 amis. Ce sont Roger et Norbert"
$moi="J'ai ".$nombre." amis. Ce sont ".$ami." et ".$ami2.".";

# $moi  contient "J'ai 2 amis. Ce sont Roger et Norbert"
$moi="J'ai $nombre amis. Ce sont $ami et $ami2.";

# $moi  contient "J'ai $nombre amis. Ce sont $ami et $ami2."
$moi='J\'ai $nombre amis. Ce sont $ami et $ami2.';


$moi.=" J'aime mes amis";
# $moi  contient "J'ai $nombre amis. Ce sont $ami et $ami2. J'aime mes amis."

?>
