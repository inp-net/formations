<?php
    
    function imprimerUnMessage($id,$auteur,$date,$titre,$contenu){
        $html=file_get_contents("templates/message.htm");
        $html=str_replace("{id}",$id,$html);
        $html=str_replace("{titre}",stripslashes($titre),$html);
        $html=str_replace("{auteur}",stripslashes($auteur),$html);
        $html=str_replace("{date}",$date,$html);
        $html=str_replace("{contenu}",stripslashes($contenu),$html);
    
        echo($html);
    }

    function imprimerFormulaire($id,$auteur,$titre,$contenu,$action="new"){
        
        $html=file_get_contents("templates/formulaire.htm");
        $html=str_replace("{id}",$id,$html);
        $html=str_replace("{action}",$action,$html);
        $html=str_replace("{titre}",stripslashes($titre),$html);
        $html=str_replace("{auteur}",stripslashes($auteur),$html);
        $html=str_replace("{contenu}",stripslashes($contenu),$html);
    
        echo($html);
    }

    function query($query){
        #Ne pas écrire en une seule ligne le "return", ça ne fonctionne pas
        $result=mysql_query($query) or die(mysql_error());
        return $result;
    }

?>
