<?php
    include("config.php");
    $db=mysql_connect(MYSQL_HOST,MYSQL_USER,MYSQL_PASSWORD);
    mysql_select_db(MYSQL_DATABASE,$db);

    include("mesFonctions.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
    <head>
        <title>Page de news</title>
        <style>
            @import url('css/style.css');
        </style>
    </head>

    <body>
    <p><a href="?new">Poster une nouvelle news</a></p>
    <p>&nbsp;</p>
    <?php
        if(isset($_GET["new"])){
            #On affiche le formulaire de création
            imprimerFormulaire(0,"","","","new");
        }
        elseif(isset($_GET["edit"])){
            #On cherche le message
            
            # On force id à etre converti en "int" s'il n'existe pas
            $id=(int)$_GET["id"];

            #On cherche dans la base mysql
            $result=query("SELECT `id`,`auteur`,`titre`,`contenu` FROM `news` where `id`=$id LIMIT 1;");

            # S'il y a une entrée c'est bon
            if(mysql_num_rows($result)>0){
                $tableau=mysql_fetch_array($result);
                imprimerFormulaire($tableau["id"],$tableau["auteur"],$tableau["titre"],$tableau["contenu"],"edit");
            }
            else{
                #Erreur, on ne peut pas éditer cette news
                echo("Erreur, la news $id n'existe pas.");
            }  
        }
        else{
        #Par défaut, on affiche toutes les news
            $result=query("SELECT * FROM `news` ORDER BY `date` DESC;");
            while($tableau=mysql_fetch_array($result)){
                imprimerUnMessage($tableau["id"],$tableau["auteur"],$tableau["date"],$tableau["titre"],$tableau["contenu"],"edit");
            }
        }

        mysql_close($db);
    
    ?>
    </body>
</html>
