\documentclass[french]{beamer}


\usepackage[utf8]{inputenc} % ~ Encodage
\usepackage[T1]{fontenc}    % ~ Encodage
\usepackage{xcolor}   % ~ Couleurs fs
\usepackage{tikz}
\usepackage{multicol}
\usepackage{xmpmulti}
\usepackage[many]{tcolorbox}
\usepackage{babel}
\usepackage{array}
\usepackage{listings}


\definecolor{darkWhite}{rgb}{0.94,0.94,0.94}

\hypersetup{
    colorlinks,
    citecolor=black,
    filecolor=black,
    linkcolor=white,
    urlcolor=blue!50!black
}

\newtcolorbox{cadre}
{
    colframe=blue!50!black,
    colback=blue!75!black,
    halign=center,
    valign=center
}

\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;}



\lstdefinelanguage{pseudocode}
{
  morekeywords={
    pour,
    dans,
    autour,
    de,
    faire,
    fin
  },
  sensitive=false, % keywords are not case-sensitive
  morecomment=[l]{//}, % l is for line comment
  morecomment=[s]{/*}{*/}, % s is for start and end delimiter
  morestring=[b]" % defines that strings are enclosed in double quotes
}

\lstset{
  %aboveskip=3mm,
  %belowskip=-2mm,
  literate=
  {é}{{\'e}}1
  {è}{{\`{e}}}1
  {ê}{{\^{e}}}1
  {ë}{{\¨{e}}}1
  {É}{{\'{E}}}1
  {Ê}{{\^{E}}}1
  {û}{{\^{u}}}1
  {ù}{{\`{u}}}1
  {â}{{\^{a}}}1
  {à}{{\`{a}}}1
  {Â}{{\^{A}}}1
  {ç}{{\c{c}}}1
  {ô}{{\^{o}}}1
  {Ô}{{\^{O}}}1
  {î}{{\^{i}}}1
  {Î}{{\^{I}}}1
  {í}{{\'{i}}}1
}

\usetheme{Dresden}
\setbeamertemplate{navigation symbols}{} 
\addtobeamertemplate{navigation symbols}{}{
    \usebeamerfont{footline}
    \hspace{1em}
    {\color{black}\insertframenumber/\inserttotalframenumber}
}

\title{Chiffrement symétrique, deux exemples~:\\ AES \& ChaCha}
\author{Nathan Maillet}
\date{Formations net7 2023/2024}

\begin{document}
\maketitle

\section*{Introduction}
\begin{frame}{Historique}
    \centering
    \textbf{Origine de l'\emph{Advanced Encryption Standard}}
    \begin{itemize}
        \item Concours NIST en 2000
        \item Rijndael approuvé
        \item But : résistant aux cryptanalyses linéaire et différentielle
        \item Fun fact : nos CPUs sont opti pour l'AES
    \end{itemize}
\end{frame}

\begin{frame}{Principes de Shannon}
    % Shannon, effet avalanche & crypto diff
    \centering
    \textbf{Recette pour faire un cryptosystème correct}
    \begin{itemize}
        \item<2-3> Diffusion : contrer les analyses statistiques,
              effacer toute \emph{redondance} avec pour but
              l'effet avalanche
              % redondance : déjà une faiblesse en soit (attaque par "répétition")
        \item<3> Confusion : relation chiffré-clé la plus complexe possible avec pour but
              le besoin d'analyser l'ensemble des clés pour tout message intercepté
              % ex : chiffre de Vigenère / César : linéaire -> similaire anti-cryptanalyse linéaire
    \end{itemize}
\end{frame}

\begin{frame}{Principes de Shannon}
    \centering
    \begin{tikzpicture}
        \draw[thick] (0,0) rectangle (2,1);
        \draw (1,0.5) node{Préimage};
        \draw[->, thick] (2,0.5) -- (3,0.5);

        \draw[thick] (3,0) rectangle (5,1);
        \draw (4,0.5) node{Confusion};
        \draw[->, thick] (5,0.5) -- (6,0.5);
        \draw[->, thick] (4,-1) -- (4,0);
        \draw (4,-1.2) node{Clé confusion};
        \draw[->, thick] (5.5,0.5) -- (5.5,1.3) -- (2.7,1.3) -- (2.7,0.5);
        \draw (4.1,1.3) node[above]{\(\alpha\) tours};

        \draw[thick] (6,0) rectangle (8,1);
        \draw (7,0.5) node{Diffusion};
        \draw[->, thick] (8,0.5) -- (9,0.5);
        \draw[->, thick] (7,-1) -- (7,0);
        \draw (7,-1.2) node{Clé diffusion};
        \draw[->, thick] (8.5,0.5) -- (8.5,2) -- (2.3,2) -- (2.3,0.5);
        \draw (5.4,2) node[above]{\(\beta\) tours};

        \draw[thick] (9,0) rectangle (11,1);
        \draw (10,0.5) node{Image};
    \end{tikzpicture}
\end{frame}
% source graphique :
% Ahmad, Jawad & Hwang, Seong. (2016). A secure image encryption scheme based on chaotic maps and affine transformation. Multimedia Tools and Applications. 75. 10.1007/s11042-015-2973-y. 

\section*{AES}
\begin{frame}{}
    \centering
    \begin{cadre}
        \textcolor{white}{
            AES
        }
    \end{cadre}
\end{frame}

\begin{frame}{SubBytes}
    \begin{columns}
        \begin{column}{0.6\linewidth}
            \centering
            \vspace*{1cm}
            \includegraphics[width=\linewidth]{Substitution.png}
        \end{column}
        \begin{column}{0.5\linewidth}
            \begin{alertblock}{Objectif}
                Créer une relation non-linéaire entre l'entrée et la sortie
            \end{alertblock}
        \end{column}
    \end{columns}
\end{frame}
% i.e. confusion + cryptanalayse linéaire

\begin{frame}{S-box}
    \centering
    \includegraphics[width=5cm]{S-box.jpg}
\end{frame}

\begin{frame}{Shift Rows}
    \centering
    \includegraphics[width=7cm]{shift_rows.png}
\end{frame}

\begin{frame}{Mix columns}
    \vspace*{0.5cm}
    \(\forall 0 \leq j \leq 3,\)
    \[
        \begin{pmatrix}
            b_{0,j} \\
            b_{1,j} \\
            b_{2,j} \\
            b_{3,j}
        \end{pmatrix}
        =
        \begin{pmatrix}
            2 & 3 & 1 & 1 \\
            1 & 2 & 3 & 1 \\
            1 & 1 & 2 & 3 \\
            3 & 1 & 1 & 2
        \end{pmatrix}
        \otimes_{\text{GF(\(2^8\))}}
        \begin{pmatrix}
            a_{0,j} \\
            a_{1,j} \\
            a_{2,j} \\
            a_{3,j}
        \end{pmatrix}
    \]
    \vspace*{1cm}
    \begin{exampleblock}{Résistance à la cryptanalyse différentielle}
        Comment augmenter sa résistance aux "timing attacks" ?
    \end{exampleblock}
\end{frame}

\begin{frame}{Gestion et extension de la clé}
    \centering
    \includegraphics[width=7cm]{AddRoundKey.png}\\
    \href{https://formaestudio.com/rijndaelinspector/archivos/Rijndael_Animation_v4_eng-html5.html}{Animation extension de la clé Rijndael (\no14)}
\end{frame}

\begin{frame}{AES}
    \centering
    \begin{columns}
        \begin{column}{5cm}
            \centering
            \includegraphics[height=6.9cm]{Structure.png}
        \end{column}
        \begin{column}{5cm}
            \begin{alertblock}{Jeu}
                Identifier, pour chaque phase, si elle ajoute de la confusion ou de la diffusion.
            \end{alertblock}
            \begin{exampleblock}{Side-channel attacks}
                Est-il viable de faire une géante table qui correspond à 1 round complet d'AES ?
            \end{exampleblock}
        \end{column}
    \end{columns}
    % Reprendre schéma Shannon
\end{frame}

\begin{frame}{Choix des rounds}
    \centering
    \begin{alertblock}{Choix des paramètres}
        Combien de rounds sommes-nous sûr d'avoir besoin pour simuler l'effet avalanche ?
    \end{alertblock}
\end{frame}

%\begin{frame}{Cryptanalyse différentielle sur de l'AES 1-round}
%\end{frame}

\section*{Modes}
\begin{frame}{}
    \centering
    \begin{cadre}
        \textcolor{white}{
            Modes cryptographiques
            % Rq: principe général au chiffrement symétrique
        }
    \end{cadre}
\end{frame}

\begin{frame}{ECB}
    \centering
    On note K la clé, M\(_i\) les blocs en clair et C\(_i\) les chiffrés
    \begin{columns}
        \begin{column}{0.6\linewidth}
            \centering
            \begin{tikzpicture}
                \draw[thick] (0,0) rectangle (2,1);
                \draw (1,0.5) node{M\(_i\)};
                \draw[->,thick] (1,0) -- (1,-1);
                \draw[thick] (0,-1) rectangle (2,-2);
                \draw (1,-1.5) node{Chiffrement};
                \draw[->,thick] (1,-2) -- (1,-3);
                \draw[thick] (0,-3) rectangle (2,-4);
                \draw (1,-3.5) node{C\(_i\)};
                \draw (-1,-0.5) node{K};
                \draw[->, thick] (-1,-0.6) -- (-1,-1.5) -- (0,-1.5);
            \end{tikzpicture}
        \end{column}
        \begin{column}{0.4\linewidth}
            \begin{alertblock}{Définition}
                En ayant \(N\) blocs à chiffré, on a :
                \[\forall j = 1..N, C_j = E(K,M_j)\]
                avec \(E\) le chiffrement choisi.
            \end{alertblock}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Objectif}
    \centering
    \begin{alertblock}{Renforcer la sécurité}
        Garder une clé pour chiffrer tous les blocs pose problème :
        deux blocs avec le même contenu seront chiffrés de la même manière.\\
        Risque d'attaque par répétition et inefficace sur les images.
    \end{alertblock}
    \includegraphics[width=2.5cm]{No_ecb_mode_picture.png}
    \includegraphics[width=2.5cm]{Ecb_mode_pic.png}
    \includegraphics[width=2.5cm]{Cbc_mode_pic.png}
\end{frame}
%source images :
%https://fr.wikipedia.org/wiki/Mode_d%27op%C3%A9ration_%28cryptographie%29

\begin{frame}{CBC}
    \begin{columns}
        \begin{column}{0.6\linewidth}
            \centering
            \begin{tikzpicture}
                \draw[thick] (0,0) rectangle (2,1);
                \draw (1,0.5) node{M\(_i\)};
                \draw[->,thick] (1,0) -- (1,-0.4);
                \draw (1,-0.5) node{\(\oplus\)};
                \draw[->, thick] (1,-0.6) -- (1,-1);
                \draw[thick] (0,-1) rectangle (2,-2);
                \draw (1,-1.5) node{Chiffrement};
                \draw[->,thick] (1,-2) -- (1,-3);
                \draw[thick] (0,-3) rectangle (2,-4);
                \draw (1,-3.5) node{C\(_i\)};
                \draw (-1,-0.5) node{K};
                \draw[->, thick] (-1,-0.6) -- (-1,-1.5) -- (0,-1.5);
                \draw[->, thick] (1,-2.5) -- (2.5,-2.5) -- (2.5,-0.5) -- (1.1,-0.5);
                \draw[right] (2.5,-1.5) node{IV si \(i = 1\)};
            \end{tikzpicture}
        \end{column}
        \begin{column}{0.4\linewidth}
            \begin{alertblock}{Définition}
                En ayant \(N\) blocs à chiffré, on a :
                \[C_1 = E(K,[P_1 \oplus \text{IV}])\]
                et, \(\forall j = 2..N :\)
                \[C_j = E(K,M_j).\]
            \end{alertblock}
        \end{column}
    \end{columns}
    \begin{exampleblock}{Question}
        Comment sécuriser l'IV ? Comment déchiffrer ?
    \end{exampleblock}
\end{frame}
% plusieurs modes : pour chiffrement par bloc/flux
% ECB peu-être utilisé pour envoyer l'IV en CBC
% CBC/ECB juste pour blocs
% autre pptés pour savoir si mode bien : parallélisables ?

\section*{ChaCha}
\begin{frame}{}
    \centering
    \begin{cadre}
        \textcolor{white}{
            ChaCha \& chiffrement par flot
        }
    \end{cadre}
\end{frame}

\begin{frame}{Chiffrement par flot}
    \centering
    \only<1>{
        \begin{itemize}
            \item On a vu le chiffrement par bloc~:
        \end{itemize}
        \begin{tikzpicture}
            \node (clair) at (0,0.5) {Clair};
            \draw[->, thick, right] (clair) -- (1,0.5);
            \draw[thick] (1,0) rectangle (3,1);
            \draw (2,0.5) node{Chiff. bloc};
            \node (chiffre) at (4,0.5) {Chiffré};
            \draw[->, thick] (3,0.5) -- (chiffre);
            \node (secret) at (2,2) {Secret};
            \draw[->, thick] (secret) -- (2,1);
        \end{tikzpicture}
    }
    \only<2>{
        \begin{itemize}
            \item On veut faire du chiffrement par flot (possible avec AES ?)
        \end{itemize}
        \begin{tikzpicture}
            \node (secret) at (0,0.5) {Secret};
            \draw[->, thick, right] (secret) -- (1,0.5);
            \draw[thick] (1,0) rectangle (3,1);
            \draw (2,0.5) node{Générateur};
            \node (chiffre) at (6,0.5) {Chiffré};
            \draw[->, thick] (3,0.5) -- (3.7,0.5);
            \node (clair) at (4,2) {Clair};
            \draw[->, thick] (clair) -- (4,0.8);
            \node (xor) at (4,0.5) {\huge \(\oplus\)};
            \draw[->, thick] (xor) -- (chiffre);
        \end{tikzpicture}
    }
    \only<3>{
        \begin{itemize}
            \item Basé sur un OTP
            \item Chiffrement plus rapide que l'AES
            \item Niveau de sécurité comparable à l'AES (présent dans TLS 1.3)
            \item Intérêt dans l'IoT/le mobile
        \end{itemize}
    }
\end{frame}

\begin{frame}{ChaCha - Quarter Round}
    \centering
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \centering
            \textbf{Etat initial}
            \resizebox{\textwidth}{!}{
                \begin{tabular}{|l|l|l|l|l|}
                    \hline & a & b & c & d\\
                    \hline a & "expa" & "nd 3" & "2-by" & "te k" \\
                    \hline b & Key & Key & Key & Key \\
                    \hline c & Key & Key & Key & Key \\
                    \hline d & Counter & Counter & IV & IV \\
                    \hline
                \end{tabular}
            }
        \end{column}
        \begin{column}{0.5\textwidth}
            \centering
            \resizebox{\textwidth}{!}{
                \includegraphics{ChaCha.png}
            }
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[fragile]{ChaCha}
\begin{lstlisting}[language=pseudocode, numbers=none]
chacha20_encrypt(key, counter, iv, plaintext):
  for j=0 upto floor(len(plaintext)/64-1):
      // chacha_qrs : 20 quarter-rounds
      key_stream = chacha_qrs(key, counter+j, none)
      ciphertext += map(xor,
                        zip(plaintext,key_stream))
  end\end{lstlisting}
\end{frame}

\section*{Bibliographie}
\begin{frame}{Bibliographie}
    \begin{itemize}
        \item Schéma sur les principes de Shannon repris de : Ahmad, Jawad \& Hwang, Seong. (2016). A secure image encryption scheme based on chaotic maps and affine transformation.
        \item Animation (très très clean) : \href{https://formaestudio.com/rijndaelinspector/archivos/Rijndael_Animation_v4_eng-html5.html}{Redirect page html}
        \item Modes, ChaCha \& vérifs constantes de l'ensemble (best website ever): \href{https://fr.wikipedia.org/wiki/Mode_d\%27op\%C3\%A9ration_\%28cryptographie\%29}{Wikipedia Modes} \& \href{https://en.wikipedia.org/wiki/Salsa20\#ChaCha_variant}{Wikipedia ChaCha}
        \item Schéma sur les modes et précisions sur l'ensemble (dispo à la biliothèque) : William Stallings. Cryptography and Network Security - Principles and Practice 7th Edition
        \item Les images et le fonctionnement de l'AES (allez sur ce site c'est une dinguerie) : \href{cryptohack.org}{cryptohack}
    \end{itemize}
\end{frame}
\end{document}
