class Voiture:
    def __init__(self):
        self.nb_roues = 4
        self.nb_places = 5


    """
    methode monte
    Enregistre un passager dans la voiture s'il y a de la place
    :param passager: String, nom du passager
    :return: boolean, True si l'opération réussie, False sinon
    """

    """
    methode descend
    Retire un passager de la voiture si sela est possible
    :return: le nom du passagé retiré ou None si c'est impossible
    """

    def __repr__(self):
        # cette fonction est appelé par python lorsque vous utilisez 'print(maVoiture)'
        return "La voiture à %d roues et %d places" % (self.nb_roues, self.nb_places)


# Programme de test de votre solution
# Executer simplement le script pour lancer le test.
# Vous ne devriez pas avoir a modifier le code suivant
if __name__ == "__main__":
    v = Voiture()
    if not v.descend():
        print("La voiture est vide.  Resultat OK")
    p = ["     Zil0", "    Zadig", "Kasonnara", "  Bitchos", "    Pibou", "   Lemeda", "   Kaname", "  Someone"]
    r = [True] * 5 + [False] * 3
    for np, nr in zip(p, r):
        print(np, "veut monter a bord. ", end="")
        s = v.monte(np)
        if s:
            print("           Et ça passe!", end="")
        else:
            print("  Mais ça ne passe pas!", end="")
        if s == nr:
            print("  Resultat OK")
        else:
            print("  Resultat FAUX")

    for k in range(6):
        s = v.descend()
        print(s, "descend!  Resultat", "OK" if (s and k<5) or (s is None and k==5) else "FAUX")


