class Destructeur:
    def __init__(self):
        self.x = 5
        self.y = 5

    def detruire(self, carte):
        if carte.is_inbound(self.x, self.y) and not carte.get_at(self.x, self.y) == ".":
            destructeur_index = carte.get_index(self.x, self.y)
            carte.carte = carte.carte[:destructeur_index] + "." + carte.carte[destructeur_index+1:]
            return True
        return False

    def __repr__(self):
        return "Le destructeur est aux coordonnées" % (self.x, self.y)

    def __str__(self):
        return "D"
