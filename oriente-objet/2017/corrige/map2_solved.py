from corrige import Vehicule2_solved


class Carte:
    def __init__(self):
        self.carte = """
######....~~~~~
....##....~~~~~
..#.#.....~~~~~
..#.......~~~~~
...........~~~~
....#......~~~~
#...#......~~~~
.......#....~~~
.#...........~~
.#.....#.......
"""
        #self.v = Vehicule2_solved.Vehicule()
        #self.v = Vehicule2_solved.Voiture()
        self.v = Vehicule2_solved.Bateau()
        #self.v = Vehicule2_solved.Formule1()
        #self.v = Vehicule2_solved.TARDIS()
    def is_inbound(self, x, y):
        return 0 <= x < 15 and 0<= y < 10

    def get_index(self, x, y):
        return x + y * 16 + 1

    def get_at(self, x, y):
        return self.carte[self.get_index(x,y)]

    def display(self):
        voiture_index = self.get_index(self.v.x, self.v.y)
        display_carte = self.carte[:voiture_index] + str(self.v) + self.carte[voiture_index+1:]
        print(display_carte)

c = Carte()

c.display()
while True:
    i = input("(t ou a) >>")
    r = False
    if i == "t":
        r = c.v.tourner()
    elif i == "a":
        r = c.v.avancer(c)
    c.display()
    if r:
        print("OK")
    else:
        print("erreur")