from django.db import models
from django.forms import ModelForm

from math import pow


class Utilisateur(models.Model):
    photo = models.ImageField(upload_to= 'girls')
    nom   = models.CharField(max_length=100)
    email = models.EmailField(default="", null=True, blank=True)
    votes = models.IntegerField(default=0, editable=False)
    score = models.FloatField(default=1000, editable=False)
    
    def comparaison(self, score, utilisateur2):
        k = 10
        if self.votes < 30:
            k = 30
        elif self.score < 2400:
            k = 15
        self.score += k * ( score - 1/(1+pow(10,-(self.score-utilisateur2.score)/400.0)) )
        self.votes += 1
    
    def __unicode__(self):
        return self.nom
    
# Facemash espionne les gens
class Votant(models.Model):
    ip       = models.GenericIPAddressField(default="0.0.0.0")
    voted    = models.ForeignKey(Utilisateur, related_name='+', on_delete=models.CASCADE)
    notVoted = models.ForeignKey(Utilisateur, related_name='+', on_delete=models.CASCADE)
    date     = models.DateTimeField() 
    
    def __unicode__(self):
        return self.ip


class UtilisateurEditForm(ModelForm):
    class Meta:
        model = Utilisateur
        fields = ('photo','nom', 'email')
