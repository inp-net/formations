from utilisateur.models import *
from django.contrib import admin

admin.site.register(Utilisateur)
admin.site.register(Votant)
