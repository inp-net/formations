from django.conf.urls.defaults import *


urlpatterns = patterns('utilisateur.views',
    (r'comparer', 'voter'),
    (r'^$','liste_utilisateurs'),
    (r'voter/(?P<user1>\d+)/(?P<user2>\d+)', 'voter'),
)

