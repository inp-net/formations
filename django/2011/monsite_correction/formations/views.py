# -*- coding: utf8 -*-

from django.shortcuts import render_to_response, get_object_or_404
from models import Formation

def accueil(request):
    c = {
        'formations': Formation.objects.all()
    }

    return render_to_response('accueil.html', c)

def liste_inscrits(request, id):
    formation = get_object_or_404(Formation, id=id)
    
    c = {
        'formation': formation,
        'liste': formation.inscription_set.all(),
    }

    return render_to_response('liste_inscrits.html', c)
