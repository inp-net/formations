# -*- coding: utf8 -*-

from django.db import models

class Formation(models.Model):
    nom = models.CharField(max_length=32)
    date = models.DateTimeField()
    
    def __unicode__(self):
        return self.nom

TYPE_INSCRIPTION = (
    (0, 'Inscrit'),
    (1, 'Présentateur'),
    (2, 'Fictif'),
)

class Inscription(models.Model):
    nom = models.CharField(max_length=32)
    type = models.SmallIntegerField(choices=TYPE_INSCRIPTION)
    formation = models.ForeignKey(Formation)
    
    def __unicode__(self):
        return u"%s (%s)" % (self.nom, self.get_type_display())
