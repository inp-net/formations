\documentclass{beamer}

\usepackage[utf8]{inputenc}
\usepackage{eso-pic}
\usepackage{graphicx}
\usepackage{minted}
\usepackage{svg}

\usetheme{Madrid}
%\usemintedstyle{tango}

\title{Formation Sécurité Applicative}
\subtitle{1 - Introduction}
\author{ Romain Malmain }
\institute{ net7 \& Pony7 }
\titlegraphic{\includegraphics[width=3cm]{net7_logo.png}\hspace*{4.75cm}~%
   \includegraphics[width=3cm]{archi_avec_contour.png}
}
\date{Octobre 2020}

\begin{document}

\AtBeginSection[]
{
  \begin{frame}
    \frametitle{Plan de la présentation}
    \tableofcontents[currentsection]
  \end{frame}
}

\frame{\titlepage}

\begin{frame}{Plan de la présentation}
\tableofcontents
\end{frame}

\section{Plan du cours}

\begin{frame}{Plan du cours}
\begin{itemize}
    \item <1-> Introduction et présentation (maintenant)
    \item <2-> Installation de l'environnement de travail
    \item <3-> Langage C / Langage assembleur
    \item <4-> Structure interne d'un programme en x86
    \item <5-> Construction d'un premier Buffer Overflow
    \item <6-> Utilisation de GDB
    \item <7-> Détournement d'exécution avec un BOF
    \item <8-> Exécution dans la pile / dans le tas : création d'un exploit de A à Z
    \item <9-> Buffer Overflow avancé
\end{itemize}
\end{frame}

\section{Fonctionnement des séances}

\begin{frame}{Séance type}
    \begin{itemize}
        \item <1->Chaque séance travaillera sur un (ou plusieurs) des points présentés précédemment
        \item <2-> Les formations seront parsemées d'exemples et de TP courts pour rendre les séances plus interactives.
            \begin{exampleblock}{TP 1.1}
                Consigne : Il faut faire...
            \end{exampleblock}
        \item <3-> Les numéros des TPs correspondront à l'arborescence dans la VM. Par exemple le TP 1.1 se trouvera dans $\sim$/Formations/securite\_applicative/1-introduction/ \\
        \item <4-> L'encadré contiendra la consigne à suivre. Habituellement les TP sont simples et rapides à faire, ils servent juste à clarifier le propos. Des TPs plus conséquents seront signalés par une couleur rouge
            \begin{alertblock}{TP 1.1}
                Consigne : Il faut faire...
            \end{alertblock}
    \end{itemize}
\end{frame}

\section{Brève explication de ce qu'est un Buffer Overflow avec quelques exemples}

\begin{frame}{Explication brève}
    \begin{block}{Définition}
        Un buffer Overflow (BOF), c'est avant tout profiter d'une erreur de programmation qui nous permet de lire ou d'écrire dans des zones auxquelles ont est pas censé accéder.
    \end{block} \pause
    \begin{exampleblock}{Exemple illustré}
        Écrire plus de caractères dans un tableau que la taille qui lui a été alloué.
    \begin{center}
        \includegraphics[scale=0.4]{overflow_schema.png}
    \end{center}
    \end{exampleblock}
\end{frame}

\begin{frame}{Exemple}
    \begin{block}{Cas typique (langage algorithmique)}
         **code**\\
        Allouer n blocs pour le tableau T\\
        Demander à l'utilisateur d'entrer une valeur et la stocker dans T (typiquement une chaîne de caractère)\\
         **code**\\
    \end{block} \pause
    \begin{exampleblock}{TP 0.1 : Effet concret d'un Buffer Overflow}
        \inputminted[fontsize=\scriptsize]{c}{1-premiere_approche.c}
    \end{exampleblock}
\end{frame}

\begin{frame}{Deuxième exemple (plus avancé)}
    \begin{exampleblock}{TP 0.2 : Ce qu'on peut arriver à faire avec un Buffer Overflow bien exploité}
        \inputminted[fontsize=\scriptsize]{c}{2-root_time.c}
    \end{exampleblock}
\end{frame}

\begin{frame}
    \begin{block}{Solution que vous serez capable d'appliquer à la fin du cours}
        \begin{itemize}
            \item <1-> Analyse du code / détection de la faille
            \item <2-> Phase de réflexion pour déterminer un bon angle d'attaque
            \item <3-> Ecriture d'un premier brouillon pour tenter d'appliquer notre idée
            \item <4-> Raffinage de la solution avec un debugger à côté pour arriver à quelque chose de fonctionnel
            \item <5-> exploiter le programme
        \end{itemize} \pause \pause \pause \pause \pause
    \end{block} 
    \begin{block}{TP 0.2 : Démonstration}
        Application d'un exploit en python écrit à la main avec le programme précédent (disponible sur le git de la formation).
    \end{block}
\end{frame}

\section{Quelques questions sur les effets d'un BOF}

\begin{frame}{Exemple}
    Quelles questions peut-on se poser ?
    \begin{itemize}
        \item <1-> Une idée de ce qu'il s'est passé ?
        \item <2-> Est-il possible de voir précisément ce qu'il s'est passé ?
        \item <3-> Si oui, quels outils peuvent nous permettre de comprendre cet effet ? Comment bien s'est servir ?
        \item <4-> Peut-on se servir de ça pour faire un peu mieux qu'un crash ?
        \item <5-> Si oui, comment s'y prendre ?
    \end{itemize}
\end{frame}

\begin{frame}{Quelques réponses...}
\begin{itemize}
    \item <1-> Beaucoup d'outils très complets pour vous aider à comprendre l'exécution d'un programme : GDB (Gnu DeBugger), radare2, ghidra, etc... Peuvent être délicat à prendre en main, mais ça vaut définitivement le coup de s'y pencher !
    \item <2-> Pour l'exploitation : Python possède beaucoup de librairies (surtout PwnTools en fait), ce qui en fait un très bon choix pour exploiter un binaire !
    \item <3-> Il existe une très grande diversité d'attaques possibles que l'on choisit en fonction de la situation (architecture processeur, sécurités actives, input possible, etc...)
    \item <4-> \textbf{Dans le cadre des formations, nous allons nous concentrer sur l'architecture x86 avec le minimum de sécurités activées.} Bien que cela ne soit pas suffisant pour compromettre des logiciels récents, cela reste un passage indispensable pour appréhender les problématiques de base liées à la sécurité applicative sans trop se prendre la tête.
\end{itemize}
\end{frame}

\section{Fin de l'introduction}

\begin{frame}{Pourquoi assister à cette formation ?}
    \begin{itemize}
        \item <1-> Le but principal est de vous permettre de comprendre les bases de la sécurité applicative pour que vous puissiez aller vers des sujets plus compliqués et éviter les erreurs pouvant compromettre vos propres programmes.
        \item <2-> Vous devriez être capable d'ici la fin de ce cours de faire la plupart des challenges simples de sécurité applicative trouvables sur internet.
        \item <3-> Comprendre le fonctionnement des buffer Overflow vous fera très certainement changer la façon dont vous voyez un programme. Le point de vue adopté est beaucoup plus "bas niveau" et permet de débugger des situations assez délicates en C par exemple, même avec une compréhension sommaire.
        \item <4-> Enfin, c'est un exercice intéressant qui donne vraiment l'impression de comprendre ce qu'il se passe une fois que ça fonctionne (assez proche d'un puzzle). Il faut souvent être persévérant et patient pour arriver au résultat final. L'exercice est assez exigeant mais satisfaisant en somme.
    \end{itemize}
\end{frame}

\begin{frame}{Fin de la séance / Questions}
    \begin{itemize}[<+->]
        \item Bibliographie : The art of exploitation (Jon Ericsson), root-me (\url{https://www.root-me.org/})
        \item La prochaine fois : Installation de la VM de travail, explication de son fonctionnement, et introduction au langage C si on a assez de temps.
        \item Retrouver tous les documents liés à la formation sur ma homepage du bde : \url{https://www.bde.enseeiht.fr/~malmair/}
        \item \textbf{Des questions ?}
    \end{itemize}
\end{frame}

\begin{frame}{Un petit extra ?}
    Un aperçu du C : \url{https://learnxinyminutes.com/docs/c/}
\end{frame}

\end{document}