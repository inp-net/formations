\documentclass{beamer}

\usepackage[utf8]{inputenc}
\usepackage{eso-pic}
\usepackage{graphicx}
\usepackage{minted}
\usepackage{svg}

\usetheme{Madrid}
%\usemintedstyle{tango}

\title{Formation Sécurité Applicative}
\subtitle{2 - Les pointeurs et la mémoire dynamique en C}
\author{ Romain Malmain }
\institute{ net7 \& Pony7 }
\titlegraphic{\includegraphics[width=3cm]{net7_logo.png}\hspace*{4.75cm}~%
   \includegraphics[width=3cm]{archi_avec_contour.png}
}
\date{16 Novembre 2020}

\begin{document}

\AtBeginSection[]
{
  \begin{frame}
    \frametitle{Plan de la présentation}
    \tableofcontents[currentsection]
  \end{frame}
}

\frame{\titlepage}

\begin{frame}{Plan de la présentation}
\tableofcontents
\end{frame}

\section{Les pointeurs en C}

\begin{frame}{Principe}
    \begin{block}{Définition}
        Un pointeur est \textbf{une variable typée qui contient une adresse vers une entité du type indiqué}. Une entité, ça peut être une autre variable, une fonction, une structure, etc...
    \end{block} \pause
        \begin{block}{Opérateurs}
        \begin{itemize}[<+->]
            \item \textit{type} \textbf{*}\textit{nom\_pointeur} : Déclaration d'une variable de type pointeur qui pointe vers une variable de type \textit{type}. \\
            \item \textit{nom\_pointeur} = \&\textit{variable} : nom\_pointeur contient l'adresse de \textit{variable}. \\
            \item \textbf{*}\textit{nom\_pointeur} = \textit{valeur} : la variable contient maintenant la valeur \textit{valeur}.
        \end{itemize}
    \end{block} \pause
    \begin{center}
        \includegraphics[scale=0.35]{pointeur_simple.jpg}
    \end{center}
\end{frame}

\begin{frame}{Exemple}
    \begin{exampleblock}{Exemple 2-1}
        \inputminted[fontsize=\tiny]{c}{2-1-exemple_pointeur.c}
    \end{exampleblock}
\end{frame}

\begin{frame}{Opérations sur les pointeurs}
    \begin{block}{Opération '+'}
        l'opération 'mon\_pointeur + i' vaut \\ \textbf{mon\_pointeur + i * sizeof(mon\_pointeur)}.
    \end{block} \pause
    \begin{exampleblock}{Exemple 2-2}
        \inputminted[fontsize=\footnotesiz]{c}{2-2-exemple_addition_pointeur.c}
    \end{exampleblock}
\end{frame}

\section{Application des pointeurs aux strings}

\begin{frame}{Les strings en C}
    \begin{block}{Définition}
        Une string est un tableau de \textit{char} terminant par le caractère nul '\\0'. Cela permet aux fonctions comme \textbf{printf} de déterminer la fin de la string. Attention, quand on déclare une string \textit{char} ma\_string[] = "salut", \textbf{ma\_string est un pointeur vers le premier caractère de la string}.
    \end{block} \pause
    \begin{center}
        \includegraphics[scale=1]{string.jpg}
    \end{center}
\end{frame}

\begin{frame}{Exemple}
    \begin{exampleblock}{Exemple 2-3}
        \inputminted[fontsize=\footnotesize]{c}{2-3-exemple_string.c}
    \end{exampleblock}
\end{frame}

\begin{frame}{Quelques remarques importantes}
    \begin{block}{Remarques}
        \begin{itemize}[<+->]
            \item L'opérateur \textbf{"..."} est équivalent à mettre ... en mémoire ET à ajouter \\0 à la fin de la string.
            \item L'opérateur \textbf{ma\_string[i]} est un raccourci pour \textbf{*(ma\_string+ i * sizeof(char))}. Généralisable à tous les types tableau, comme int[], etc....
        \end{itemize}
    \end{block}
\end{frame}

\section{La mémoire allouée dynamiquement}

\begin{frame}{Situations qui posent problème}
    \begin{block}{Quelques programmes classiques}
        \begin{itemize}[<+->]
            \item Enregistrer n entiers dans un tableau, avec \textbf{n demandé au début du programme}
            \item Créer un tableau qui change de taille pendant l'exécution du programme
            \item Demander à l'utilisateur de rentrer son prénom sans à priori sur sa taille (le prénom peut être aussi long qu'il le souhaite)
            \item etc...
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}{Solution : la mémoire dynamique}
    \begin{block}{Utilisation}
        \begin{itemize}[<+->]
        \item \textbf{(void *) malloc(size\_t)} Renvoie un pointeur vers une zone en mémoire contenant autant d'octets que le nombre indiqué en paramètre. C'est au programmeur de typer le pointeur en fonction de ce qui va être stocké !
        \item \textbf{(void *) realloc(void *, size\_t)} Renvoie un pointeur vers une zone en mémoire contenant autant d'octets que le nombre indiqué en paramètre. Il faut également lui indiquer un pointeur vers l'ancienne zone mémoire (permet de l'agrandir / rétrécir).
        \item \textbf{free(void *)} Libère la zone mémoire indiquée en paramètre (allouée avec malloc / realloc / etc...). Ne renvoie rien.
    \end{itemize}
    \end{block}
\end{frame}

\begin{frame}{Exemple}
    \begin{exampleblock}{Exemple 2-4}
        \inputminted[fontsize=\tiny]{c}{2-4-exemple_memoire_dynamique.c}
    \end{exampleblock}
\end{frame}

\begin{frame}{Conseils / erreurs à éviter}
    \begin{block}{Utilisation}
        \begin{itemize}[<+->]
        \item Ne pas réutiliser un pointeur après l'utilisation de free ! (use after free)
        \item Ne pas dépasser la taille allouée dans malloc ou realloc
        \item ... Et d'autres règles pleine de bon sens
        \end{itemize}
    \end{block}
\end{frame}
    
\begin{frame}{Liste plus exhaustive}
    \begin{center}
        \includegraphics[scale=0.3]{heap-rules-CS.png}
    \end{center}
    Source de l'image : Azeria labs $\longrightarrow$ \url{https://azeria-labs.com}.
\end{frame}

\begin{frame}{La pratique}
    \begin{exampleblock}{TP - Créer une pile dynamique}
        Dans le répertoire de la formation dans la VM (dossier 2-c\_asm), on va faire le premier exercice de la deuxième série et plus si possible :)
    \end{exampleblock}
\end{frame}

\end{document}