#include <stdio.h>

int add(int x, int y) {
    return x + y;
}

int main() {
    int x = 1;
    int y = 2;
    return add(x, y);
}
