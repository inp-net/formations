#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void copy(char *arg)
{
	char msg[256];
	strcpy(msg, arg);
    printf("Votre argument est : %s\n",msg);
}

int main(int argc, char **argv)
{
	if (argc != 2)
	{
		printf("Usage : prog argv1\n");
		exit(1);
	}

	copy(argv[1]);
	return 0;
}
